# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w(epic/saga family business).each do |name|
  Subject.create(name: name)
end

epic = Subject.find_by_name('epic/saga')
3.times do
  epic.books.create title: Faker::Book.title
end

family = Subject.find_by_name('family')
2.times do
  family.books.create title: Faker::Book.title
end

book = Book.first
book.subjects << family

family_book = Book.last
family_book.subjects << Subject.find_by_name('business')
family_book.subjects << Subject.find_by_name('epic/saga')
